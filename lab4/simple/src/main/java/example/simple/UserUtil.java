package example.simple;

import java.util.UUID;

public class UserUtil {
    public String generateId() {
        return UUID.randomUUID().toString();
    }
}
