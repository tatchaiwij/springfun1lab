package example.simple;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSetter_DI {
    public static void main(String[] args) {

        try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml")) {
            UserService service = (UserService)context.getBean("userService");
            service.showId();
        }
}

}