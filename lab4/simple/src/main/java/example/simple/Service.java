package example.simple;

public class Service {
    private Util util;

    public Service(Util util) {
        this.util = util;
    }
    public void speak(String message){
        util.speak(message);
    }
}
