package example.simple;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestConstructor_DI {
    public static void main(String[] args){

        try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml")){
                Service service = (Service)context.getBean("service");
                service.speak("I love you");
        }
    }
}
