package example.simple;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSimple {

    public static void main(String[] args){
        
        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml")){
            Person person = (Person)context.getBean("person");
            System.out.println(person.getFullName());
        }
    }
}